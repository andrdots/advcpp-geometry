// SPDX-License-Identifier: LGPL-2.1-or-later
/*
 * Authors:
 * * Andrey N. Dotsenko
 *
 * Copyright (c) 2023 Andrey N. Dotsenko
 */

#include <advcpp/circle.h>

using namespace advcpp;
using namespace advcpp::circle;

template
in4D::Point<double> GetCenter<double>(const in4D::Point<double> &start, const in4D::Point<double> &middle, const in4D::Point<double> &end);
