// SPDX-License-Identifier: MIT-0
/*
 * Authors:
 * * Andrey N. Dotsenko
 *
 * Copyright (c) 2023–2024 Andrey N. Dotsenko
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom
 * the Software is furnished to do so.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <advcpp/ellipse.h>

#include <gtest/gtest.h>

using namespace advcpp;
using namespace in4D;

using ::testing::InitGoogleTest;

TEST(CircleTest, GetAngleByPoint)
{
    Point center(1.0, 1.0, 1.0);
    Vector majorDir(1.0, 0.0, 0.0);
    Vector normal(0.0, 0.0, 1.0);

    Point p(1.0 + 5.0, 1.0, 1.0);
    auto angle = ellipse::GetParameterAngleByPoint(p, center, normal, majorDir, 5.0, 2.0);
    ASSERT_NEAR(angle, 0.0, 0.0001);

    p.x = 1.0;
    p.y = 1.0 + 2.0;
    angle = ellipse::GetParameterAngleByPoint(p, center, normal, majorDir, 5.0, 2.0);
    ASSERT_NEAR(angle, M_PI / 2, 0.0001);

    p.x = 1.0 - 5.0;
    p.y = 1.0;
    angle = ellipse::GetParameterAngleByPoint(p, center, normal, majorDir, 5.0, 2.0);
    ASSERT_NEAR(angle, M_PI, 0.0001);

    p.x = 1.0;
    p.y = 1.0 - 2.0;
    angle = ellipse::GetParameterAngleByPoint(p, center, normal, majorDir, 5.0, 2.0);
    ASSERT_NEAR(angle, 3 * M_PI / 2, 0.0001);

    p.x = 1.0 + 3.5355339059327378;
    p.y = 1.0 + 1.414213562373095;
    angle = ellipse::GetParameterAngleByPoint(p, center, normal, majorDir, 5.0, 2.0);
    ASSERT_NEAR(angle, M_PI / 4, 0.0001);
}

TEST(CircleTest, GetTangentByParameterAngle)
{
    Vector majorDir(1.0, 0.0, 0.0);
    Vector normal(0.0, 0.0, 1.0);

    Vector tangent = ellipse::GetTangentByParameterAngle(normal, majorDir, 5.0, 2.0, M_PI_4);
    ASSERT_NEAR(tangent.x, -0.9284766908852593, 0.0001);
    ASSERT_NEAR(tangent.y, 0.3713906763541037, 0.0001);

    tangent = ellipse::GetTangentByParameterAngle(normal, majorDir, 5.0, 2.0, M_PI + M_PI_4);
    ASSERT_NEAR(tangent.x, 0.9284766908852593, 0.0001);
    ASSERT_NEAR(tangent.y, -0.3713906763541037, 0.0001);

    normal *= -1;

    tangent = ellipse::GetTangentByParameterAngle(normal, majorDir, 5.0, 2.0, M_PI_4);
    ASSERT_NEAR(tangent.x, -0.9284766908852593, 0.0001);
    ASSERT_NEAR(tangent.y, -0.3713906763541037, 0.0001);

    tangent = ellipse::GetTangentByParameterAngle(normal, majorDir, 5.0, 2.0, M_PI + M_PI_4);
    ASSERT_NEAR(tangent.x, 0.9284766908852593, 0.0001);
    ASSERT_NEAR(tangent.y, 0.3713906763541037, 0.0001);
}

TEST(CircleTest, ComputeQuadrantLength)
{
    double len = ellipse::ComputeQuadrantLength(10.0, 5.0);
    ASSERT_NEAR(len, 12.110560275684591, 0.0001);

    len = ellipse::ComputeQuadrantLength(0.1, 0.2);
    ASSERT_NEAR(len, 0.24221120551369194, 0.0001);

    len = ellipse::ComputeQuadrantLength(1000.0, 5000.0);
    ASSERT_NEAR(len, 5252.511134922251, 0.0001);

    len = ellipse::ComputeQuadrantLength(100000.0, 500000.0);
    ASSERT_NEAR(len, 525251.1134922251, 0.0001);
}

TEST(CircleTest, IntegrateArcLength)
{
    double len = ellipse::IntegrateArcLength(10.0, 5.0, 0.0, M_PI * 2);
    ASSERT_NEAR(len, 48.44224110273838, 0.0001);

    len = ellipse::IntegrateArcLength(0.1, 0.2, 0.0, M_PI * 2);
    ASSERT_NEAR(len, 0.9688448220547676, 0.0001);

    len = ellipse::IntegrateArcLength(1000.0, 5000.0, 0.0, M_PI * 2);
    ASSERT_NEAR(len, 21010.044539689006, 0.0001);

    len = ellipse::IntegrateArcLength(10.0, 5.0, M_PI_4, M_PI_2);
    ASSERT_NEAR(len, 7.282241554573458, 0.0001);

    len = ellipse::IntegrateArcLength(5.0, 3.0, 3.14159, 3.15144);
    ASSERT_NEAR(len, 0.029550848761111803, 0.0001);
}

TEST(CircleTest, ComputeArcLength)
{
    double len = ellipse::ComputeArcLength(10.0, 5.0, 0.0, M_PI * 2);
    ASSERT_NEAR(len, 48.44224110273838, 0.0001);

    len = ellipse::ComputeArcLength(0.1, 0.2, 0.0, M_PI * 2);
    ASSERT_NEAR(len, 0.9688448220547676, 0.0001);

    len = ellipse::ComputeArcLength(1000.0, 5000.0, 0.0, M_PI * 2);
    ASSERT_NEAR(len, 21010.044539689006, 0.0001);

    len = ellipse::ComputeArcLength(100000.0, 500000.0, 0.0, M_PI * 2);
    ASSERT_NEAR(len, 2101004.4539689007, 0.0001);

    len = ellipse::ComputeArcLength(10.0, 5.0, M_PI / 4, M_PI);
    ASSERT_NEAR(len, 19.392801830258055, 0.0001);

    len = ellipse::ComputeArcLength(10.0, 5.0, 0.0, M_PI - M_PI / 4);
    ASSERT_NEAR(len, 19.39280183025805, 0.0001);
}

TEST(CircleTest, ComputeArcLength_smallArc)
{
    double len = ellipse::ComputeArcLength(5.0, 3.0, 3.14523, 3.14887);
    ASSERT_LT(len, 0.5);
}

int
main(int argc, char **argv)
{
    InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
