// SPDX-License-Identifier: MIT-0
/*
 * Authors:
 * * Andrey N. Dotsenko
 *
 * Copyright (c) 2023 Andrey N. Dotsenko
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom
 * the Software is furnished to do so.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <advcpp/circle.h>

#include <gtest/gtest.h>

using namespace advcpp;
using namespace in4D;

using ::testing::InitGoogleTest;

TEST(CircleTest, GetCenterBy3Points)
{
    Point p1(1.0, 1.0, 0.0);
    Point p2(6.0, 4.0, 0.0);
    Point p3(11.0, 1.0, 0.0);

    Point c = circle::GetCenter(p1, p2, p3);
    ASSERT_NEAR(c.x, 6, 0.001);
    ASSERT_NEAR(c.y, -1.666, 0.001);
    ASSERT_NEAR(c.z, 0, 0.001);
}

TEST(CircleTest, GetArcLen)
{
    Point p1(0.0, 1.0, 0.0);
    Point p2(1.0, 0.0, 0.0);
    Point c(0.0, 0.0, 0.0);

    Vector n = (c - p1) * (c - p2);
    double angle = circle::GetArcAngle(p1, p2, c, n);
    ASSERT_NEAR(angle, M_PI / 2, 0.001);

    n *= -1;
    angle = circle::GetArcAngle(p1, p2, c, n);
    ASSERT_NEAR(angle, M_PI * 3 / 2, 0.001);

    p1 = Point(1.0, 1.0, 0.0);
    p2 = Point(6.0, 4.0, 0.0);
    Point p3(11.0, 1.0, 0.0);

    c = circle::GetCenter(p1, p2, p3);

    n = (c - p1) * (c - p3);
    angle = circle::GetArcAngle(p1, p3, c, n);
    ASSERT_NEAR(angle, 120 * M_PI / 180, 0.1);

    n *= -1;
    angle = circle::GetArcAngle(p1, p3, c, n);
    ASSERT_NEAR(angle, 240 * M_PI / 180, 0.1);
}

TEST(CircleTest, GetArcPointByAngle)
{
    Point p1(1.0, 1.0, 0.0);
    Point p2(6.0, 4.0, 0.0);
    Point p3(11.0, 1.0, 0.0);

    Point c = circle::GetCenter(p1, p2, p3);

    Vector n = (c - p1) * (c - p3);
    n.normalize();
    double angle = circle::GetArcAngle(p1, p3, c, n);
    ASSERT_NEAR(angle, 120 * M_PI / 180, 0.1);

    Point p = circle::GetArcPointByAngle(p1, c, n, angle);
    ASSERT_NEAR(p.x, p3.x, 0.01);
    ASSERT_NEAR(p.y, p3.y, 0.01);
    ASSERT_NEAR(p.z, p3.z, 0.01);
}

int
main(int argc, char **argv)
{
    InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
