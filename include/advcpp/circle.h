// SPDX-License-Identifier: LGPL-2.1-or-later
/*
 * Authors:
 * * Andrey N. Dotsenko
 *
 * Copyright (c) 2023 Andrey N. Dotsenko
 */

#ifndef ADVCPP_GEOMETRY_CIRCLE_H_
#define ADVCPP_GEOMETRY_CIRCLE_H_

#include <advcpp/Matrix.h>

#include <iostream>

namespace advcpp
{

namespace circle
{

template<typename T>
in4D::Point<T>
GetCenter(const in4D::Point<T> &start, const in4D::Point<T> &middle, const in4D::Point<T> &end);

template<typename T>
in4D::Point<T>
GetCenter(const in4D::Point<T> &start, const in4D::Point<T> &middle, const in4D::Point<T> &end)
{
    in4D::Vector<T> AB = middle - start;
    in4D::Vector<T> AC = end - start;
    in4D::Vector<T> u = AB;
    in4D::Vector<T> w = u * AC;
    in4D::Vector<T> v = u * w;
    u.normalize();
    v.normalize();
    w.normalize();

    in4D::Point<T> nB(AB.projLen(u), 0.0, 0.0);
    in4D::Point<T> nC(AC.projLen(u), AC.projLen(v), 0.0);
    in4D::Point<T> nCenter(
        nB.x / 2.0,
        (nC.x * (nC.x - nB.x) / (nC.y - nB.y) + nB.y + nC.y) / 2.0,
        0.0);

    return start + (u * nCenter.x) + (v * nCenter.y);
}

template<typename T>
double
GetArcAngle(
    const in4D::Point<T> &start,
    const in4D::Point<T> &end,
    const in4D::Point<T> &center,
    const in4D::Vector<T> normal)
{
    in4D::Vector<T> AC = center - start;
    in4D::Vector<T> BC = center - end;
    T d = AC.dot(BC);
    T angle = acos(d / (AC.len() * BC.len()));

    in4D::Vector<T> dirV = AC * normal;
    T projLen = BC.projLen(dirV);
    if (projLen > 0) {
        angle = 2 * M_PI - angle;
    }
    return angle;
}

template<typename T>
in4D::Point<T>
GetArcPointByAngle(
    const in4D::Point<T> &start,
    const in4D::Point<T> &center,
    const in4D::Vector<T> normal,
    T angle)
{
    in4D::Matrix<T> m = in4D::Matrix<T>::Rotation(normal, angle);
    in4D::Vector<T> AC = start - center;
    return center + AC * m;
}

template<typename T>
in4D::Vector<T>
GetArcTangentByAngle(
    const in4D::Point<T> &start,
    const in4D::Point<T> &center,
    const in4D::Vector<T> normal,
    T angle)
{
    in4D::Matrix<T> m = in4D::Matrix<T>::Rotation(normal, angle);
    in4D::Vector<T> AC = start - center;
    return ((AC * m) * normal).normalized();
}

template<typename T>
in4D::Vector<T>
GetArcTangentByAngle(
    const in4D::Point<T> &start,
    const in4D::Point<T> &center,
    const in4D::Vector<T> normal,
    T angle,
    T angleUp)
{
    in4D::Matrix<T> m = in4D::Matrix<T>::Rotation(normal, angle);
    in4D::Vector<T> AC = start - center;
    in4D::Vector<T> tangent = normal * (AC * m);
    in4D::Matrix<T> mUp = in4D::Matrix<T>::Rotation(AC.normalized(), angleUp);
    tangent = tangent * mUp;
    return tangent.normalized();
}

} // namespace circle

} // namespace advcpp

#endif /* ADVCPP_GEOMETRY_CIRCLE_H_ */
