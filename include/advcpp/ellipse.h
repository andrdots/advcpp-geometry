// SPDX-License-Identifier: LGPL-2.1-or-later
/*
 * Authors:
 * * Andrey N. Dotsenko
 *
 * Copyright (c) 2023–2024 Andrey N. Dotsenko
 */

#ifndef ADVCPP_GEOMETRY_ELLIPSE_H_
#define ADVCPP_GEOMETRY_ELLIPSE_H_

#include <advcpp/Matrix.h>

#include <algorithm>

namespace advcpp
{

namespace ellipse
{

template<typename T>
T
GetParameterAngleByPoint(
    const in4D::Point<T> &point,
    const in4D::Point<T> &center,
    const in4D::Vector<T> &normal,
    const in4D::Vector<T> &majorDir,
    T majorRadius,
    T minorRadius)
{
    in4D::Vector<T> v = point - center;
    in4D::Vector<T> minorDir = normal * majorDir;
    in4D::Point<T> p(v.projLen(majorDir), v.projLen(minorDir), 0.0);
    T angle = atan2(p.y * majorRadius, p.x * minorRadius);
    if (angle < 0) {
        angle = 2 * M_PI + angle;
    }
    return angle;
}

template<typename T>
in4D::Vector<T>
GetTangentByParameterAngle(
    const in4D::Vector<T> &normal,
    const in4D::Vector<T> &majorDir,
    T majorRadius,
    T minorRadius,
    T parameterAngle)
{
    in4D::Vector<T> minorDir = normal * majorDir;
    in4D::Vector<T> planeTangent(
        -majorRadius * sin(parameterAngle),
        minorRadius * cos(parameterAngle),
        0.0);
    in4D::Vector<T> tangent = planeTangent.x * majorDir + planeTangent.y * minorDir;
    tangent.normalize();
    return tangent;
}

template<typename T>
T
ComputeQuadrantLength(T majorRadius, T minorRadius, int iterationsCount = 11)
{
    if (minorRadius > majorRadius) {
        std::swap(minorRadius, majorRadius);
    }

    T e2 = 1.0 - sqr(minorRadius) / sqr(majorRadius);
    T a = 1.0;
    T b = minorRadius / majorRadius;
    T s = e2;
    int p = 1;
    for (int i = 0; i < iterationsCount; ++i) {
        T ai = (a + b) / 2.0;
        T bi = sqrt(a * b);
        a = ai;
        b = bi;
        p *= 2;
        s += p * (sqr(a) - sqr(b));
    }

    T k = M_PI / (2 * a);

    return majorRadius * k * (1.0 - 0.5 * s);
}

template<typename T>
T
IntegrateArcLength(
    T majorRadius,
    T minorRadius,
    T startParamAngle,
    T endParamAngle,
    T iterationsCount = 0.0)
{
    int count = iterationsCount;
    if (count == 0.0) {
        T maxRadius = std::max(majorRadius, minorRadius);
        constexpr int um_in_mm = 1000;
        count = round((maxRadius * 2 * M_PI * um_in_mm) / 10);
    }

    T d = (endParamAngle - startParamAngle) / count;
    if (d <= std::numeric_limits<T>::epsilon()) {
        return 0;
    }

    T len = 0;
#pragma omp parallel for reduction(+ : len)
    for (int i = 0; i < count; i += 1) {
        T angle = startParamAngle + d * i + d / 2;
        T q = sqrt(sqr(majorRadius * sin(angle)) + sqr(minorRadius * cos(angle)));
        len += q;
    }
    len *= d;
    return len;
}

template<typename T>
T
ComputeArcLength(
    T majorRadius,
    T minorRadius,
    T startParamAngle,
    T endParamAngle,
    int iterationsCount = 11)
{
    T angle1Int;
    T angle1Frac = modf(startParamAngle / M_PI_2, &angle1Int);
    if (fabs(angle1Frac) <= std::numeric_limits<T>::epsilon()) {
        angle1Frac = 0;
    }
    T angle2Int;
    T angle2Frac = modf(endParamAngle / M_PI_2, &angle2Int);
    if (fabs(angle2Frac) <= std::numeric_limits<T>::epsilon()) {
        angle2Frac = 0;
    }
    if ((angle1Frac > 0) && (angle1Int != angle2Int)) {
        ++angle1Int;
    }
    T quadrantsCount = fabs(angle2Int - angle1Int);
    T len = 0;
    if (quadrantsCount == 0) {
        return IntegrateArcLength(majorRadius, minorRadius, startParamAngle, endParamAngle);
    }

    len += quadrantsCount * ComputeQuadrantLength(majorRadius, minorRadius, iterationsCount);
    if ((angle1Frac == 0) && (angle2Frac == 0)) {
        return len;
    }

    // FIXME: use approximation to compute incomplete elliptic integral
    if (angle1Frac != 0) {
        len += IntegrateArcLength(
            majorRadius,
            minorRadius,
            startParamAngle,
            startParamAngle + angle1Frac * M_PI_2);
    }

    if (angle2Frac != 0) {
        len += IntegrateArcLength(
            majorRadius,
            minorRadius,
            endParamAngle - angle2Frac * M_PI_2,
            endParamAngle);
    }

    return len;
}

template<typename T>
std::pair<T, T>
ComputeApproxParameterAngleByLen(
    T majorRadius,
    T minorRadius,
    T startAngle,
    T expectedLen,
    T precision = 0.001,
    T dAngle = 0)
{
    if (dAngle == 0) {
        dAngle = expectedLen / ((majorRadius + minorRadius) / 2);
    }
    T angle = startAngle;
    T len = 0;

    T dLen = 0;
    T expectedDLen = expectedLen - len;
    char prevOp = 0;
    T k = 2;
    while ((dLen <= expectedDLen - precision) || (dLen >= expectedDLen + precision)) {
        dLen = IntegrateArcLength(majorRadius, minorRadius, angle, angle + dAngle, 1.0);
        if (dLen <= expectedDLen - precision) {
            if (prevOp == '/') {
                k = 1 + (k - 1) / 2;
            }
            dAngle *= k;
            prevOp = '*';
        } else if (dLen >= expectedDLen + precision) {
            if (prevOp == '*') {
                k = 1 + (k - 1) / 2;
            }
            dAngle /= k;
            prevOp = '/';
        }
    }
    return std::make_pair<>(dAngle, dLen);
}

template<typename T>
in4D::Point<T>
GetPointByParameterAngle(
    T angle,
    const in4D::Point<T> &center,
    const in4D::Vector<T> &majorDir,
    const in4D::Vector<T> &minorDir,
    T majorRadius,
    T minorRadius)
{
    in4D::Point<T> p(majorRadius * cos(angle), minorRadius * sin(angle), 0.0);
    p = center + p.x * majorDir + p.y * minorDir;
    return p;
}

} // namespace ellipse

} // namespace advcpp

#endif // ADVCPP_GEOMETRY_ELLIPSE_H_
