# advcpp-geometry

## Name
Full name: Advanced C++ Geometry.

Short name: advcpp-geometry.

## Description
The library provides basic operations on geometric primitives.

## Authors
* Andrey N. Dotsenko

## License
The source code is licensed under the GNU Lesser General Public License, version 2.1 or later license.
